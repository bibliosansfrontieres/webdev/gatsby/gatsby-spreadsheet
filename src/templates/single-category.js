import React from "react";
import { withStyles } from "@material-ui/core/styles";
import { graphql } from "gatsby";
import Typography from "@material-ui/core/Typography";
import Chip from "@material-ui/core/Chip";
import Badge from '@material-ui/core/Badge';
//import Icon from '@material-ui/core/Icon';
import Layout from "../components/layout";
import PageCard from "../components/PageCard";

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
  flexBoxParentDiv: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    flexFlow: "row wrap",
    justifyContent: "space-between",
    "&::after": {
      content: "",
      flex: "auto",
    },
  },
  categoryChips: {
    borderRadius: 3,
    maxWidth: 350 - 30,
  },

})

const CategoryPage = ({ data, classes, pageContext }) => {
  const itemsWithCategory = data.allGoogleSpreadsheetExport.edges

  return (
    <Layout>
      <div className={classes.root}>
        <Typography variant="h4" gutterBottom align="center" component="h1">
          <Badge badgeContent={itemsWithCategory.length} color="primary">
          <Chip 
          //icon={<Icon>{itemsWithCategory.icon}</Icon>}
          label={pageContext.category} 
          key={pageContext.category.toString()} 
          variant="outlined"
          color="secondary"
          className={classes.categoryChips}/>
          </Badge>
        </Typography>
        <div className={classes.flexBoxParentDiv}>
          {itemsWithCategory.map((item, index) => (
          <PageCard item={item} key={index.toString()} />
          ))}
        </div>
      </div>
    </Layout>
  )
}

export default withStyles(styles)(CategoryPage)

export const categoryPageQuery = graphql`
  query CategoryPage($category: String) {
    site {
      siteMetadata {
        title
      }
    }
    allGoogleSpreadsheetExport(filter: { category: { in: [$category] } }) {
      totalCount
      edges {
        node {
          id
          optimized_thumbnail {
            childImageSharp {
              fluid(maxWidth: 400, maxHeight: 250) {
                ...GatsbyImageSharpFluid
              }
            }
          }
          title
          category
          icon
          tags
          location
          servicesProvided
          optimized_location_image {
            childImageSharp {
              fluid(maxHeight: 50) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`
