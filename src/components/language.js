import React from "react";
import { Link } from "gatsby"
import { IntlContextConsumer, changeLocale } from "gatsby-plugin-intl";

const languageName = {
  en: "English",
  ar: "العربية",
  fr: "Français",
}

const Language = () => {
  return (
    <div>
      <IntlContextConsumer>
        {({ languages, language: currentLocale }) =>
          languages.map(language => (
            // add true to attribute href as recommended to pass it to the dom
            <Link to="/" href="true"
              key={language}
              onClick={() => changeLocale(language)}
              style={{
                color: currentLocale === language ? `black` : `white`,
                margin: 5,
              }}
            >
              {languageName[language]}
            </Link>
          ))
        }
      </IntlContextConsumer>
    </div>
  )
}

export default Language
