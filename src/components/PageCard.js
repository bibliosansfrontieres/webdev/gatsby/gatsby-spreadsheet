import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import Chip from "@material-ui/core/Chip";
import Typography from "@material-ui/core/Typography";
import { Link } from "gatsby";
import Avatar from "@material-ui/core/Avatar";
import LocationOnIcon from "@material-ui/icons/LocationOn";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";
import Icon from "@material-ui/core/Icon";
import Divider from "@material-ui/core/Divider";
import Img from "gatsby-image";
import { kebabCase } from "lodash";
const styles = theme => ({
  card: {
    width: 310,
    margin: 10,
    [theme.breakpoints.down('sm')]: {
      width: "98vw",
    },
  },
  cardContent: {
    height: 230,
    [theme.breakpoints.down('sm')]: {
      height:180
    },
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  title: {
    lineHeight: 1.2,
    fontSize: "1.2rem",
  },
  avatar: {
    backgroundColor: "#f47725",
  },
  tagChips: {
    margin: "0px 3px 0px 0px",
    marginBottom: 10,
  },
  rtlCompat: {
    marginRight: "2px",
  },
  locationChips: {
    marginLeft: 17,
    background: "none",
    border: "none",
    marginRight: "4px",
    maxWidth: 300 - 30,
  },
  categoryChips: {
    background: "#fafafa",
    borderRadius: 3,
    marginTop: 10,
    maxWidth: 300 - 30,
  },
})


function PageCard(props) {
  const { classes, item } = props
  return (
    <Card className={classes.card}>
      <CardActionArea>
        <Link
          style={{ textDecoration: "none" }}
          to={`/card/${kebabCase(item.node.title)}/`}
        >
          <CardHeader
            avatar={
              <Avatar className={classes.avatar}>
                {item.node.title.charAt(0).toUpperCase()}
              </Avatar>
            }
            title={item.node.title.toUpperCase()}
          />
        </Link>
          <Link
            to={`/location/${kebabCase(item.node.location)}/`}
            style={{ textDecoration: "none" }}
          >
            <Chip
              icon={
                <LocationOnIcon/>
              }
              label={item.node.location}
              variant="outlined"
              color="secondary"
              clickable
              className={classes.locationChips}
            />
          </Link>
          {console.log(item.node)}
           <Img key={item.node.optimized_location_image}
            alt={item.node.title}
            fluid={item.node.optimized_location_image.childImageSharp.fluid}
          /> 
        <Divider />
      </CardActionArea>
      <CardContent>
        {item.node.tags.split(",").map((tag, i) => (
          <Link
            to={`/tag/${kebabCase(tag)}/`}
            style={{ textDecoration: "none" }}
            key={i.toString()}
          >
            <Chip
              icon={<LocalOfferIcon className={classes.rtlCompat} />}
              clickable
              variant="outlined"
              color="secondary"
              size="small"
              label={tag}
              key={tag.toString()}
              className={classes.tagChips}
            />
          </Link>
        ))}
        <Typography variant="body2" color="textSecondary" component="p">
          {item.node.servicesProvided}
        </Typography>
        <Link
          to={`/category/${kebabCase(item.node.category)}/`}
          style={{ textDecoration: "none" }}
        >
          <Chip
            icon={
              <Icon className={classes.rtlCompat} >{item.node.icon}</Icon>
            }
            label={item.node.category}
            variant="outlined"
            color="secondary"
            size="small"
            clickable
            className={classes.categoryChips}
          />
        </Link>
      </CardContent>
    </Card>
  )
}

export default withStyles(styles)(PageCard)
