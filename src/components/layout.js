import React from "react";
import { StaticQuery, graphql } from "gatsby";
import { injectIntl } from "gatsby-plugin-intl";
import Header from "./header";
import Footer from "./footer";
import Theme from "../../static/utils/theme";
import { ThemeProvider } from "@material-ui/styles";
import "./layout.css";

const Layout = ({ children, intl }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    
    render={data => (
        <>
            <ThemeProvider theme={Theme}>
                <Header 
                    title={data.site.siteMetadata.title}
                    intl={intl}
                />
                <div className="site"
                    style={{
                    margin: `0 auto`,
                    maxWidth: 1060,
                    padding: `.5rem`,
                    paddingTop: ".5rem",
                    }}
                >
                <div className="site-content">{children}</div>
                    <Footer intl={intl} />
                </div>
            </ThemeProvider>
        </>
    )}
  />
);

export default injectIntl(Layout)
